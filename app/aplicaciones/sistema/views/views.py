from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from aplicaciones.sistema.forms import *
# from proyecto.app.aplicaciones.sistema.forms import *
from aplicaciones.sistema.models import *
# from proyecto.app.aplicaciones.sistema.models import *


def raza_list(request):
    data = {
        'title': 'Listado de Raza',
        'raza': Raza.objects.all()
    }
    return render(request, 'raza/list.html', data)


def post(request, *args, **kwargs):
    data = {}
    try:
        data = Raza.objects.get(pk=request.POST['id']).toJSON()
    except Exception as e:
        data['error'] = str(e)
    return JsonResponse(data)


class RazaListView(ListView):
    model = Raza
    template_name = 'raza/list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Raza'
        return context


class RazaCreateView(CreateView):
    model = Raza
    form_class = RazaForm
    template_name = 'raza/create.html'
    success_url = reverse_lazy('aplicaciones.sistema:raza_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Agregar una Raza'
        return context


class RazaUpdateView(UpdateView):
    model = Raza
    form_class = RazaForm
    template_name = 'raza/create.html'
    success_url = reverse_lazy('aplicaciones.sistema:raza_list')

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición una Raza'
        context['entity'] = 'Raza'
        context['list_url'] = reverse_lazy('aplicaciones.sistema:raza_list')
        context['action'] = 'edit'
        return context


class RazaDeleteView(DeleteView):
    model = Raza
    template_name = 'raza/delete.html'
    success_url = reverse_lazy('erp:category_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de una Raza'
        context['entity'] = 'Razas'
        context['list_url'] = reverse_lazy('aplicaciones.sistema:raza_list')
        return context


def vientre_list(request):
    data = {
        'title': 'Listado de Vientre',
        'vientre': Vientre.objects.all()
    }
    return render(request, 'vientre/list.html', data)


class VientreListView(ListView):
    model = Vientre
    template_name = 'vientre/list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Vientre'
        return context


def palpacion_list(request):
    data = {
        'title': 'Listado de Palpacion',
        'palpacion': Palpacion.objects.all()
    }
    return render(request, 'palpacion/list.html', data)


class PalpacionListView(ListView):
    model = Palpacion
    template_name = 'palpacion/list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Palpacion'
        return context


def animal_list(request):
    data = {
        'title': 'Listado de Animal',
        'animal': Animal.objects.all()
    }
    return render(request, 'animal/list.html', data)


class AnimalListView(ListView):
    model = Animal
    template_name = 'animal/list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Animal'
        return context


# def perdida_list(request):
#     data = {
#         'title': 'Listado de Pérdidas',
#         'perdida': Perdida.objects.all()
#     }
#     return render(request, 'perdida/list.html', data)
#
#
# class PerdidaListView(ListView):
#     model = Perdida
#     template_name = 'perdida/list.html'
#
#     @method_decorator(login_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         data = {}
#         try:
#             data = Perdida.objects.get(pk=request.POST['id']).toJSON()
#         except Exception as e:
#             data['error'] = str(e)
#         return JsonResponse(data)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['title'] = 'Listado de Pérdidas'
#         return context
#
#
# def nacimiento_list(request):
#     data = {
#         'title': 'Listado de Nacimientos',
#         'nacimiento': Nacimiento.objects.all()
#     }
#     return render(request, 'nacimiento/list.html', data)
#
#
# class NacimientoListView(ListView):
#     model = Nacimiento
#     template_name = 'nacimiento/list.html'
#
#     @method_decorator(login_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         data = {}
#         try:
#             data = Nacimiento.objects.get(pk=request.POST['id']).toJSON()
#         except Exception as e:
#             data['error'] = str(e)
#         return JsonResponse(data)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['title'] = 'Listado de Nacimientos'
#         return context
#
#
# def fallecimiento_list(request):
#     data = {
#         'title': 'Listado de Fallecimientos',
#         'fallecimiento': Fallecimiento.objects.all()
#     }
#     return render(request, 'fallecimiento/list.html', data)
#
#
# class FallecimientoListView(ListView):
#     model = Fallecimiento
#     template_name = 'fallecimiento/list.html'
#
#     @method_decorator(login_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         data = {}
#         try:
#             data = Fallecimiento.objects.get(pk=request.POST['id']).toJSON()
#         except Exception as e:
#             data['error'] = str(e)
#         return JsonResponse(data)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['title'] = 'Listado de Fallecimientos'
#         return context
#
#
# def marcado_list(request):
#     data = {
#         'title': 'Listado de Marcados',
#         'raza': Marcado.objects.all()
#     }
#     return render(request, 'marcado/list.html', data)
#
#
# class MarcadoListView(ListView):
#     model = Marcado
#     template_name = 'marcado/list.html'
#
#     @method_decorator(login_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         data = {}
#         try:
#             data = Marcado.objects.get(pk=request.POST['id']).toJSON()
#         except Exception as e:
#             data['error'] = str(e)
#         return JsonResponse(data)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['title'] = 'Listado de Marcados'
#         return context
#
#
# def medida_list(request):
#     data = {
#         'title': 'Listado de Medidas',
#         'medida': Medida.objects.all()
#     }
#     return render(request, 'medida/list.html', data)
#
#
# class MedidaListView(ListView):
#     model = Medida
#     template_name = 'medida/list.html'
#
#     @method_decorator(login_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         data = {}
#         try:
#             data = Medida.objects.get(pk=request.POST['id']).toJSON()
#         except Exception as e:
#             data['error'] = str(e)
#         return JsonResponse(data)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['title'] = 'Listado de Medidas'
#         return context
#
#
# def tipo_tprev_list(request):
#     data = {
#         'title': 'Listado de Tipos de Tratamiento Preventivo',
#         'tipo_tprev': TipoTPrev.objects.all()
#     }
#     return render(request, 'tipo_tprev/list.html', data)
#
#
# class TipoTPrevListView(ListView):
#     model = TipoTPrev
#     template_name = 'tipo_tprev/list.html'
#
#     @method_decorator(login_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         data = {}
#         try:
#             data = TipoTPrev.objects.get(pk=request.POST['id']).toJSON()
#         except Exception as e:
#             data['error'] = str(e)
#         return JsonResponse(data)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['title'] = 'Listado de Tipos de Tratamiento Preventivo'
#         return context
#
#
# def tprev_list(request):
#     data = {
#         'title': 'Listado de Tratamientos Preventivos',
#         'tprev': TPreventivo.objects.all()
#     }
#     return render(request, 'tprev/list.html', data)
#
#
# class TPreventivoListView(ListView):
#     model = TPreventivo
#     template_name = 'tprev/list.html'
#
#     @method_decorator(login_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         data = {}
#         try:
#             data = TPreventivo.objects.get(pk=request.POST['id']).toJSON()
#         except Exception as e:
#             data['error'] = str(e)
#         return JsonResponse(data)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['title'] = 'Listado de Tratamientos Preventivos'
#         return context
#
#
# def tcurativo_list(request):
#     data = {
#         'title': 'Listado de Tratamientos Curativos',
#         'tcurativo': TCurativo.objects.all()
#     }
#     return render(request, 'tcurativo/list.html', data)
#
#
# class TCurativoListView(ListView):
#     model = TCurativo
#     template_name = 'tcurativo/list.html'
#
#     @method_decorator(login_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         data = {}
#         try:
#             data = TCurativo.objects.get(pk=request.POST['id']).toJSON()
#         except Exception as e:
#             data['error'] = str(e)
#         return JsonResponse(data)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['title'] = 'Listado de Tratamientos Curativos'
#         return context
#
#
# def aplicacion_tprev_list(request):
#     data = {
#         'title': 'Listado de Aplicaciones de Tratamientos Preventivos',
#         'aplicacion_tprev': AplicacionTP.objects.all()
#     }
#     return render(request, 'aplicacion_tprev/list.html', data)
#
#
# class AplicacionTPrevListView(ListView):
#     model = AplicacionTP
#     template_name = 'aplicacion_tprev/list.html'
#
#     @method_decorator(login_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         data = {}
#         try:
#             data = AplicacionTP.objects.get(pk=request.POST['id']).toJSON()
#         except Exception as e:
#             data['error'] = str(e)
#         return JsonResponse(data)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['title'] = 'Listado de Aplicaciones de Tratamientos Preventivos'
#         return context
#
#
# def comprador_list(request):
#     data = {
#         'title': 'Listado de Compradores',
#         'comprador': Comprador.objects.all()
#     }
#     return render(request, 'comprador/list.html', data)
#
#
# class CompradorListView(ListView):
#     model = Comprador
#     template_name = 'comprador/list.html'
#
#     @method_decorator(login_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         data = {}
#         try:
#             data = Comprador.objects.get(pk=request.POST['id']).toJSON()
#         except Exception as e:
#             data['error'] = str(e)
#         return JsonResponse(data)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['title'] = 'Listado de Compradores'
#         return context
#
#
# def lote_venta_list(request):
#     data = {
#         'title': 'Listado de Lotes para Venta',
#         'raza': LoteVenta.objects.all()
#     }
#     return render(request, 'lote_venta/list.html', data)
#
#
# class LoteVentaListView(ListView):
#     model = LoteVenta
#     template_name = 'lote_venta/list.html'
#
#     @method_decorator(login_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         data = {}
#         try:
#             data = LoteVenta.objects.get(pk=request.POST['id']).toJSON()
#         except Exception as e:
#             data['error'] = str(e)
#         return JsonResponse(data)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['title'] = 'Listado de Lotes para Venta'
#         return context
#
#
# def animales_lote_list(request):
#     data = {
#         'title': 'Listado de Animales por Lote',
#         'animales_lote': AnimalesLote.objects.all()
#     }
#     return render(request, 'animales_lote/list.html', data)
#
#
# class AnimalesLoteListView(ListView):
#     model = AnimalesLote
#     template_name = 'animales_lote/list.html'
#
#     @method_decorator(login_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         data = {}
#         try:
#             data = AnimalesLote.objects.get(pk=request.POST['id']).toJSON()
#         except Exception as e:
#             data['error'] = str(e)
#         return JsonResponse(data)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['title'] = 'Listado de Animales por Lote'
#         return context
#
#
# def articulo_list(request):
#     data = {
#         'title': 'Listado de Artículos',
#         'articulo': Articulo.objects.all()
#     }
#     return render(request, 'articulo/list.html', data)
#
#
# class ArticuloListView(ListView):
#     model = Articulo
#     template_name = 'articulo/list.html'
#
#     @method_decorator(login_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         data = {}
#         try:
#             data = Articulo.objects.get(pk=request.POST['id']).toJSON()
#         except Exception as e:
#             data['error'] = str(e)
#         return JsonResponse(data)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['title'] = 'Listado de Artículos'
#         return context
#
#
# def compra_list(request):
#     data = {
#         'title': 'Listado de Compras',
#         'compra': Compra.objects.all()
#     }
#     return render(request, 'compra/list.html', data)
#
#
# class CompraListView(ListView):
#     model = Compra
#     template_name = 'compra/list.html'
#
#     @method_decorator(login_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         data = {}
#         try:
#             data = Compra.objects.get(pk=request.POST['id']).toJSON()
#         except Exception as e:
#             data['error'] = str(e)
#         return JsonResponse(data)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['title'] = 'Listado de Compras'
#         return context
#
#
# def detalle_compra_list(request):
#     data = {
#         'title': 'Listado de Detalles de Compra',
#         'detalle_compra': DetalleCompra.objects.all()
#     }
#     return render(request, 'detalle_compra/list.html', data)
#
#
# class DetalleCompraListView(ListView):
#     model = DetalleCompra
#     template_name = 'detalle_compra/list.html'
#
#     @method_decorator(login_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         data = {}
#         try:
#             data = DetalleCompra.objects.get(pk=request.POST['id']).toJSON()
#         except Exception as e:
#             data['error'] = str(e)
#         return JsonResponse(data)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['title'] = 'Listado de Detalle de Compra'
#         return context
