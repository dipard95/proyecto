from django.db import models
from datetime import datetime
from django.forms import model_to_dict


class Raza(models.Model):
    descripcion = models.CharField(max_length=100, verbose_name='Descripción', unique=True)

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha de Creación')

    #    operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.descripcion

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Raza'
        verbose_name_plural = 'Razas'
        db_table = 'raza'
        ordering = ['id']


class Animal(models.Model):
    # tipoAnimal = models.ForeignKey(tipoAnimal, on_delete=models.PROTECT, verbose_name='Tipo Animal')
    raza = models.ForeignKey(Raza, on_delete=models.PROTECT, verbose_name='Raza')
    id_Animal = models.CharField(max_length=50, verbose_name='Id Animal', unique=True)
    fecha_nacimiento = models.DateField(default=datetime.now, verbose_name='Fecha Nacimiento')
    op_grupo_destino = (
        ('E', 'ENGORDE'),
        ('R', 'REPRODUCCION')
    )
    grupo_destino = models.CharField(max_length=1, choices=op_grupo_destino, default='E', verbose_name='Grupo Destino')
    op_sexo = (
        ('M', 'MASCULINO'),
        ('F', 'FEMENINO')
    )
    sexo = models.CharField(max_length=1, choices=op_sexo, default='M', verbose_name='Sexo')
    op_estado = (
        ('V', 'VIVO'),
        ('M', 'MUERTO'),
        ('E', 'VENDIDO')
    )
    estado = models.CharField(max_length=1, choices=op_estado, default='V',
                              verbose_name='Estado')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    #    operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id_Animal

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Animal'
        verbose_name_plural = 'Animales'
        db_table = 'animal'
        ordering = ['id']


class Vientre(models.Model):
    id_madre = models.ForeignKey(Animal, on_delete=models.PROTECT, related_name='madre', verbose_name='Madre')
    id_padre = models.ForeignKey(Animal, on_delete=models.PROTECT, related_name='padre', verbose_name='Padre')

    fecha = models.DateField(default=datetime.now, verbose_name='Fecha')

    #    operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.id)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Vientre'
        verbose_name_plural = 'Vientre'
        db_table = 'vientre'
        ordering = ['id']


class Palpacion(models.Model):
    id_vientre = models.ForeignKey(Vientre, on_delete=models.PROTECT, verbose_name='Vientre')
    op_resultado = (
        ('E', 'EXITO'),
        ('F', 'PERDIDA')
    )
    resultado = models.CharField(max_length=1, choices=op_resultado, default='E', verbose_name='Resultado')
    observacion = models.CharField(max_length=100, verbose_name='Observación', null=True, blank=True)

    fecha = models.DateField(default=datetime.now, verbose_name='Fecha')

    #    operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.id)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Palapcion'
        verbose_name_plural = 'Palapacion'
        db_table = 'palpacion'
        ordering = ['id']


class Perdida(models.Model):
    id_Palpacion = models.ForeignKey(Palpacion, on_delete=models.PROTECT, verbose_name='Palpacion')
    causa = models.CharField(max_length=100, verbose_name='Causa')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Perdida'
        verbose_name_plural = 'Perdidas'
        db_table = 'perdida'
        ordering = ['id']


class Nacimiento(models.Model):
    id_Vientre = models.ForeignKey(Vientre, on_delete=models.PROTECT, verbose_name='Vientre')
    # Agregar el ID del animal una vez creado el registro.
    id_Animal = models.ForeignKey(Animal, on_delete=models.PROTECT, verbose_name='Animal')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Nacimiento'
        verbose_name_plural = 'Nacimientos'
        db_table = 'nacimiento'
        ordering = ['id']


class Fallecimiento(models.Model):
    id_Animal = models.ForeignKey(Animal, on_delete=models.PROTECT, verbose_name='Animal')
    causa = models.CharField(max_length=100, verbose_name='Causa', null=False)

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Fallecimiento'
        verbose_name_plural = 'Fallecimientos'
        db_table = 'fallecimiento'
        ordering = ['id']


class Marcado(models.Model):
    id_Animal = models.ForeignKey(Animal, on_delete=models.PROTECT, verbose_name='Animal')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Marcado'
        verbose_name_plural = 'Marcados'
        db_table = 'marcado'
        ordering = ['id']


class Medida(models.Model):
    id_Animal = models.ForeignKey(Animal, on_delete=models.PROTECT, verbose_name='Animal')
    pesoKG = models.IntegerField(verbose_name='Peso en Kg')
    alturaCM = models.IntegerField(verbose_name='Altura en cm')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Medida'
        verbose_name_plural = 'Medidas'
        db_table = 'medida'
        ordering = ['id']


class TipoTPrev(models.Model):
    nombreTipo = models.CharField(max_length=100, verbose_name='Nombre Tipo')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'TipoTPrev'
        verbose_name_plural = 'TipoTPrevs'
        db_table = 'tipo_tprev'
        ordering = ['id']


class TCurativo(models.Model):
    id_Animal = models.ForeignKey(Animal, on_delete=models.PROTECT, verbose_name='Animal')
    observacion = models.CharField(max_length=100, verbose_name='Observacion')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'TCurativo'
        verbose_name_plural = 'TCurativos'
        db_table = 'tcurativo'
        ordering = ['id']


class TPreventivo(models.Model):
    id_TipoTPrev = models.ForeignKey(TipoTPrev, on_delete=models.PROTECT, verbose_name='TipoTPrev')
    nombre = models.CharField(max_length=100, verbose_name='Nombre', null=False)
    fechaInicio = models.DateField(default=datetime.now, verbose_name='Fecha Inicio', null=False)
    periodicidad = models.IntegerField(verbose_name='Periodicidad')
    observacion = models.CharField(max_length=100, verbose_name='Observacion')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'TPreventivo'
        verbose_name_plural = 'TPreventivos'
        db_table = 'tpreventivo'
        ordering = ['id']


class AplicacionTP(models.Model):
    id_Animal = models.ForeignKey(Animal, on_delete=models.PROTECT, verbose_name='Animal')
    id_TPreventivo = models.ForeignKey(TPreventivo, on_delete=models.PROTECT, verbose_name='TPreventivo')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'AplicacionTP'
        verbose_name_plural = 'AplicacionTPs'
        db_table = 'aplicacion_tp'
        ordering = ['id']


class Comprador(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', null=False)
    telefono = models.CharField(max_length=100, verbose_name='Telefono', null=False)
    mail = models.CharField(max_length=100, verbose_name='Mail', null=False)

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Comprador'
        verbose_name_plural = 'Compradores'
        db_table = 'comprador'
        ordering = ['id']


class LoteVenta(models.Model):
    id_Comprador = models.ForeignKey(Comprador, on_delete=models.PROTECT, verbose_name='Comprador')
    op_estado = (
        ('P', 'PREPARADO'),
        ('E', 'ENVIADO'),
        ('C', 'CONFIRMADO')
    )
    estado = models.CharField(max_length=1, choices=op_estado, default='P', verbose_name='Estado')
    fecha_envio = models.DateField(verbose_name='Fecha Envío')
    fecha_confirmado = models.DateField(verbose_name='Fecha Confirmado')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'LoteVenta'
        verbose_name_plural = 'LoteVentas'
        db_table = 'lote_venta'
        ordering = ['id']


class AnimalesLote(models.Model):
    id_Animal = models.ForeignKey(Animal, on_delete=models.PROTECT, verbose_name='Animal')
    id_Lote = models.ForeignKey(LoteVenta, on_delete=models.PROTECT, verbose_name='Lote Venta')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'AnimalesLote'
        verbose_name_plural = 'AnimalesLotes'
        db_table = 'animales_lote'
        ordering = ['id']


class Articulo(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', null=False)
    cantidadActual = models.IntegerField(verbose_name='Cantidad Actual', default=0, null=False)
    cantidadIdeal = models.IntegerField(verbose_name='Cantidad Ideal', default=0, null=False)

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Articulo'
        verbose_name_plural = 'Articulos'
        db_table = 'articulo'
        ordering = ['id']


class Compra(models.Model):
    fechaCompra = models.DateField(default=datetime.now, verbose_name='Fecha Compra')
    totalCompra = models.IntegerField(verbose_name='Total Compra')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Compra'
        verbose_name_plural = 'Compras'
        db_table = 'compra'
        ordering = ['id']


class DetalleCompra(models.Model):
    id_Compra = models.ForeignKey(Compra, on_delete=models.PROTECT, verbose_name='Compra')
    id_Articulo = models.ForeignKey(Articulo, on_delete=models.PROTECT, verbose_name='Articulo')
    cantidad = models.IntegerField(verbose_name='Cantidad', null=False)

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'DetalleCompra'
        verbose_name_plural = 'DetalleCompras'
        db_table = 'detalle_compra'
        ordering = ['id']
