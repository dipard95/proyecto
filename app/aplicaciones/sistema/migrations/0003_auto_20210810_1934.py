# Generated by Django 3.2.6 on 2021-08-10 23:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sistema', '0002_auto_20210806_1728'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='animal',
            name='fecha_creacion',
        ),
        migrations.RemoveField(
            model_name='animal',
            name='fecha_nacimineto',
        ),
        migrations.RemoveField(
            model_name='raza',
            name='fecha_creacion',
        ),
        migrations.RemoveField(
            model_name='tipoanimal',
            name='fecha_creacion',
        ),
    ]
