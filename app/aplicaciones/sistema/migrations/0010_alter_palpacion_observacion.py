# Generated by Django 3.2.6 on 2021-08-17 20:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sistema', '0009_palpacion_vientre'),
    ]

    operations = [
        migrations.AlterField(
            model_name='palpacion',
            name='observacion',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Observación'),
        ),
    ]
