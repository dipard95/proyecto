# Generated by Django 3.2.6 on 2021-09-13 15:03

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sistema', '0015_aplicaciontp_tpreventivo'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comprador',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100, verbose_name='Nombre')),
                ('telefono', models.CharField(max_length=100, verbose_name='Telefono')),
                ('mail', models.CharField(max_length=100, verbose_name='Mail')),
                ('fecha_creacion', models.DateField(default=datetime.datetime.now, verbose_name='Fecha Creación')),
            ],
            options={
                'verbose_name': 'Comprador',
                'verbose_name_plural': 'Compradores',
                'db_table': 'comprador',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='LoteVenta',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('estado', models.CharField(choices=[('P', 'PREPARADO'), ('E', 'ENVIADO'), ('C', 'CONFIRMADO')], default='P', max_length=1, verbose_name='Estado')),
                ('fecha_envio', models.DateField(verbose_name='Fecha Envío')),
                ('fecha_confirmado', models.DateField(verbose_name='Fecha Confirmado')),
                ('fecha_creacion', models.DateField(default=datetime.datetime.now, verbose_name='Fecha Creación')),
                ('id_Comprador', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='sistema.comprador', verbose_name='Comprador')),
            ],
            options={
                'verbose_name': 'LoteVenta',
                'verbose_name_plural': 'LoteVentas',
                'db_table': 'lote_venta',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='AnimalesLote',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_creacion', models.DateField(default=datetime.datetime.now, verbose_name='Fecha Creación')),
                ('id_Animal', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='sistema.animal', verbose_name='Animal')),
                ('id_Lote', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='sistema.loteventa', verbose_name='Lote Venta')),
            ],
            options={
                'verbose_name': 'AnimalesLote',
                'verbose_name_plural': 'AnimalesLotes',
                'db_table': 'animales_lote',
                'ordering': ['id'],
            },
        ),
    ]
