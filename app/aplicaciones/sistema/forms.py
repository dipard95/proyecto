from django.forms import ModelForm, TextInput
from aplicaciones.sistema.models import *

class RazaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            form.field.widget.attrs['class'] = 'form-control'
            form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['descripcion'].widget.attrs['autofocus'] = True

    class Meta:
        model = Raza
        fields = '__all__'
        exclude = ['fecha_creacion']
        widgets = {
            'descripcion': TextInput(
                attrs={
                    'placeholder': 'Ingrese un descripción de la Raza',
                }
            ),
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data
